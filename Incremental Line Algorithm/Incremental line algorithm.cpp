// Incremental line algorithm.cpp

#include<stdio.h>
#include<conio.h>
#include<graphics.h>
#include<math.h>

void lineIncremental(int x1, int y1, int x2, int y2)
{
	int x;
	float dy = y2 - y1;
	float dx = x2 - x1;
	float m = dy/dx;
	float y=y1;
	
	if((m>-1)||(m<1))
	{
		for(x=x1;x<=x2;x++)
		{
			putpixel(x, floor(1+y), BLUE);
			y+=m;	
		}		
	}
	else
	{
		for(x=y1;x<=y2;x++)
		{
			putpixel(x, floor(1+y), BLUE);
			x+=1/m;
		}
	}
}

main()
{
	int gd=DETECT, gm;
	int x1, y1, x2, y2;
	printf("Enter the numbers:\n");
	printf("x1=");
	scanf("%d", &x1);
	printf("y1=");
	scanf("%d", &y1);
	printf("x2=");
	scanf("%d", &x2);
	printf("y2=");
	scanf("%d", &y2);	
	initgraph(&gd, &gm, "\\tc");
	lineIncremental(x1,y1,x2,y2);
//	lineIncremental(100,400,300,50);
	getch();
}






