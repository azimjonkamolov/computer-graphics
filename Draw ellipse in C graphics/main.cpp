// Author: Azimojon Kamolov
// Purpose: C Implementation for drawing ellipse 

#include <graphics.h>
  
int main() 
{ 
    // gm is Graphics mode which is a computer display 
    // mode that generates image using pixels. 
    // DETECT is a macro defined in "graphics.h" header file 
    int gd = DETECT, gm; 
  
    // location of ellipse 
    int x, y;
    
    printf("Enter the location of ellipse x and y respectively: ");
    scanf("%d %d", &x, &y);
  
    // here is the starting angle 
    // and end angle 
    int start_angle; 
    int end_angle; 
    
    printf("Enter the starting angle and end angle: ");
    scanf("%d %d", &start_angle, &end_angle);
  
    // radius from x axis and y axis 
    int x_rad; 
    int y_rad; 
    
    printf("Enter the radius from x axis and y axis: ");
    scanf("%d %d", &x_rad, &y_rad);
  
    // initgraph initializes the graphics system 
    // by loading a graphics driver from disk 
    initgraph(&gd, &gm, ""); 
  
    // ellipse fuction 
    ellipse(x, y, start_angle, 
     end_angle, x_rad, y_rad); 
  
    getch(); 
  
    // closegraph function closes the graphics 
    // mode and deallocates all memory allocated 
    // by graphics system . 
    closegraph(); 
  
    return 0; 
} 

