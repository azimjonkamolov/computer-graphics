// Line program with Incremental algorithm with comments.c
#include<stdio.h>
#include<graphics.h>
#include<math.h>

void lineIncremental(int x1, int y1, int x2, int y2) // to create line dots using measures
{
	int x;
	float dy = y2-y1;
	float dx = x2-x1;
	float m = dy/dx; // if m>1 x1 goes to x2, else y1 goes to y2
	float y = y1;
	
	if((m>-1) || (m<1))
	{
		for(x=x1; x<=x2; x++) // in this case x x1 goes to x2 giving \ direction
		{
			putpixel(x, floor(0.5+y), WHITE);
			y+=m; // adding m to y
		}
	}
	else // in the opposite case
	{
		for(x=y1;x<=y2;x++) // y1 goes to y2 giving / direction
		{
			putpixel(x, floor(0.5+y), WHITE);
			x+=1/m; // adding 1/m to x in this situation
		}
	}
}
main() // to call and use main func no int or void is used
{
	int gd=DETECT, gm; //graphics.h library is necessary here
	int x1, x2, y1, y2; // declare int and input the values for them as integer date
	initgraph(&gd, &gd, "\\tc"); // graphics library is necessay for this case too
	printf("Enter the numbers as asked:\n"); // 
	printf("x1= ");
	scanf("%d", &x1);
	printf("y1= ");
	scanf("%d", &y1);
	printf("x2= ");
	scanf("%d", &x2);
	printf("y2= ");
	scanf("%d", &y2);
	lineIncremental(x1,y1,x2,y2); // call the function
	lineIncremental(y1,x1,y2,x2); // call the funciton again
	getch(); // hold here until the another input is inserted :()
	return 0;
}










